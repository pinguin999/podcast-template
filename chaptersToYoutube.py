import sys

fileName = sys.argv[1]


class Chapter(object):
    def __init__(self, title, start, end):
        self.title = title
        self.start = start
        self.end = end

    def format(self):
        minutes = int(self.start // 60)
        seconds = int(self.start - (minutes * 60))
        return "%d:%02d %s" % (minutes, seconds, self.title)


def _readChapters(fileName):
    with open(fileName) as f:
        for line in f.readlines():
            elements = line.split("\t")
            yield Chapter(elements[2].strip(), float(elements[0]), float(elements[1]))


for chapter in _readChapters(fileName):
    print(chapter.format())
