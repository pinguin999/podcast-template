---
_template: _layouts/post.html
title:     "A First Blog Post"
date:      2023-06-08 23:26:00 +0200
author:    Carsten Pfeffer
blog:      true
---

All blog posts reside in the `blog` directory. You can experiment with it and rebuild the website by typing `rattlesnake` into a terminal or `rattlesnake --watch`.
This additionally watches for changes and rebuilds the website. Further, it serves it on port 8000.

To add new posts, simply add a file in the `blog` directory. The file name will be the last part of the URL to the blog post. the file should have a front matter containing a reference to the template used for rendering the post, a title, date, author and the information that this is a blog post. Take a look at the source for this post to get an idea about how it works.
