---
_template: _layouts/page.html
title: Episodes
nav_order: 2
---

<center>
{% set episodes = _pages|selectattr("podcast")|sort(attribute="date", reverse=True) %}
{% for episode in episodes %}
<a class="post-link episode-box" href="{{ website_baseurl }}{{ episode._target_path }}">
    <img src="{{ episode.cover }}" class="cover" />
    <br/>
    <span class="post-meta">{{ episode.date | format_date("%d.%m.%Y") }} - Episode {{ episode.number }}</span>
    <br/>
    {{ episode.title }}
</a>
{% endfor %}
</center>
