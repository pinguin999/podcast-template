---
_template: _layouts/page.html
title: Blog
nav_order: 3
---

<p class="rss-subscribe">Subscribe <a href="{{ website_baseurl }}/feed.xml">via RSS</a></p>

<ul class="post-list">
{% for post in _pages|selectattr("blog")|sort(attribute="date", reverse=True) %}
    <li>
    <span class="post-meta">{{ post.date | format_date("%b %-d, %Y") }}</span>

    <h2>
        <a class="post-link" href="{{ website_baseurl }}{{ post._target_path }}">{{ post.title }}</a>
    </h2>
    </li>
{% endfor %}
</ul>
