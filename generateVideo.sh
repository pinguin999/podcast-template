# $1: image file
# $2: mp3 file
# $3: output file

if [ "$#" -ne 3 ]; then
    echo "Usage: ./generateVideo.sh image.png audio.mp3 output.mp4"
else
    ffmpeg -loop 1 -i $1 -i $2 -acodec copy -shortest -vcodec libx264 -framerate 1 -tune stillimage $3
fi
