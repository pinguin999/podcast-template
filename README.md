# How to host a Podcast for free with GitLab Pages and Rattlesnake

With this template you can host a podcast and a blog for free using GitLab pages.

## How to set up
First fork the template project (it used to be based on the official *Jekyll* template https://gitlab.com/pages/jekyll, but is now replaced by [Rattlesnake](https://pac4.gitlab.io/rattlesnake/) which is based on *Python* instead of *Ruby*).

In `config.yml` you can set your meta information for the podcast.

## How Rattlesnake works
Rattlesnake is a static website generator (like *Jekyll*). It takes Markdown, HTML and SCSS files and renders them into HTML and CSS that can be served statically by any web server. You can find more information about Rattlesnake here: https://pac4.gitlab.io/rattlesnake/
TL;DR: put your blog posts as markdown files in `blog`. 

## How to add episodes
An episode requires five files in total. The naming convention for those files is simply the number of the episode with three digits and leading zeroes (e.g. 000.xyz, 001.xyz, 002.xyz, etc.).

### Cover art (XXX.png)
First, you need a cover art image for the episode. The cover art images are located in the coverArt directory and in the PNG format (coverArt/XXX.png). A resolution of
 1400 x 1400 pixels is recommended for the most podcasting platforms.

The other files must be located in the _episodes_src/ directory:

### The audio file (XXX.mp3)
The audio content of the episode. It should not contain any metadata, since this is added during the CI build.


### The metadata (XXX.json)
The metadata for the episode is stored in a JSON file. The properties of the JSON object are the following:

"title" -- the title of the episode
"subtitle" -- a subtitle
"summary" -- a short summary of the episode contents (not more than a paragraph)
"date" -- the date the episode was recorded or published
"youtube" -- (optional) a link to the YouTube video (if the episode was published on YouTube)

An example could look like the following:

```
{
	"title" : "The libido of ants",
	"subtitle": "Hot insights into a fascinating microcosm",
	"summary": "How do ants reproduce? What do female ants like on their male counterparts? Our expert guest John Doe will surprise you!",
	"date": "2021-04-05 10:00",
	"youtube": "f0ws2BARt9v"
}
```

### The Episode Page (XXX.md)
The episode page (often called show notes) is described in a markdown file. The rendered page first contains the episode title and subtitle (from the JSON file), the podlove web player and the contents of this file.

### The chapter marks (XXX.chapters)
The chapter marks are read from a file that follows the Audacity format for time labels. If you are using Audacity for recording, you can add a label track via [Tracks]->[Add new]->[Label Track] and add your labels with [Ctr]+[B]. The labels can then be exported via [File]->[Export}->[Export Labels].
If you are not using audacity, you can create the file manually or via script. The format is as follows:

`[start seconds][tab][end seconds][tab][label title]`

An example looks as follows:

```
119.423141    119.423141    Introduction Carsten
144.066743    144.066743    Introduction Kolja
160.457012    160.457012    First Global Game Jam
```

Note that for the chapter marks the start and end timestamps are always the same.
